# Types

- null?
- error?

## Pair

- normal pair
- optimized pair?
- arguments?

## Scalar

- character
- integer
- rational
- io state
- module

## Array

- generic array
- static string

## Function

- generic function (array of specialized functions?)
- builtin function
- projection
- hook?
- condition?
- delay?
